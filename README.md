# Group 5 - lab1

Instructions: Run the app. The default language for the app is english. Have the app running in the background, open up Settings -> System -> Languages & Input -> Languages -> + Add a language -> español (Mexico) -> drag "español (Mexico)" to be first on the list of languages -> Go back to app

This will automatically translate the app.
