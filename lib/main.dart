import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'app_localizations.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Localization',
      // List all of the app's supported locales here
      supportedLocales: [
        Locale('en', 'US'),
        Locale('es', 'MX'),
      ],
      // These delegates make sure that the localization data for the proper language is loaded
      localizationsDelegates: [
        // A class which loads the translations from JSON files
        AppLocalizations.delegate,
        // Built-in localization of basic text for Material widgets
        GlobalMaterialLocalizations.delegate,
        // Built-in localization for text direction LTR/RTL
        GlobalWidgetsLocalizations.delegate,
      ],
      // Returns a locale which will be used by the app
      localeResolutionCallback: (locale, supportedLocales) {
        // Check if the current device locale is supported
        for (var supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode &&
              supportedLocale.countryCode == locale.countryCode) {
            return supportedLocale;
          }
        }
        // If the locale of the device is not supported, use the first one
        // from the list (English, in this case).
        return supportedLocales.first;
      },
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[700],
          title: FittedBox(fit:BoxFit.fitWidth,
            child: Text(
              AppLocalizations.of(context).translate('title'),
              style: TextStyle(
                  fontFamily: 'Georgia-Bold',
                  fontSize: 36
              ),
            ),
          ),
      ),
      backgroundColor: Colors.blue[50],
      body: SingleChildScrollView (
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      child: Text(
                        AppLocalizations.of(context).translate('supported'),
                        style: TextStyle(
                            color: Colors.grey[800],
                            fontFamily: 'Helvetica',
                            fontSize: 18
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: Icon(Icons.info_outline),
                        onPressed: () {
                          showAlertDialog1(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Text(
                AppLocalizations.of(context).translate('string_one'),
                style: TextStyle(
                    color: Colors.blue[700],
                    fontFamily: 'Helvetica-Bold',
                    fontSize: 27
                ),
              ),
              SizedBox(height: 10),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      child: Text(
                        AppLocalizations.of(context).translate('string_two'),
                        style: TextStyle(
                            fontFamily: 'Helvetica',
                            fontSize: 18
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: Icon(Icons.info_outline),
                        onPressed: () {
                          showAlertDialog2(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      child: Text(
                        AppLocalizations.of(context).translate('string_three'),
                        style: TextStyle(
                            fontFamily: 'Helvetica',
                            fontSize: 18
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: Icon(Icons.info_outline),
                        onPressed: () {
                          showAlertDialog3(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 30),
              Text(
                AppLocalizations.of(context).translate('string_four'),
                style: TextStyle(
                    color: Colors.blue[700],
                    fontFamily: 'Helvetica-Bold',
                    fontSize: 27
                ),
              ),
              SizedBox(height: 10),
              Text(
                AppLocalizations.of(context).translate('string_five'),
                style: TextStyle(
                    fontFamily: 'Helvetica',
                    fontSize: 18
                ),
              ),
              SizedBox(height: 10),
              Text(
                AppLocalizations.of(context).translate('string_six'),
                style: TextStyle(
                    fontFamily: 'Helvetica',
                    fontSize: 18
                ),
              ),
              SizedBox(height: 10),
              Text(
                AppLocalizations.of(context).translate('string_seven'),
                style: TextStyle(
                    fontFamily: 'Helvetica',
                    fontSize: 18
                ),
              ),
              SizedBox(height: 30),
              Text(
                AppLocalizations.of(context).translate('string_eight'),
                style: TextStyle(
                    color: Colors.blue[700],
                    fontFamily: 'Helvetica-Bold',
                    fontSize: 27
                ),
              ),
              SizedBox(height: 10),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      child: Text(
                        AppLocalizations.of(context).translate('string_nine'),
                        style: TextStyle(
                            fontFamily: 'Helvetica',
                            fontSize: 18
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: Icon(Icons.info_outline),
                        onPressed: () {
                          showAlertDialog2(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 30),
              Text(
                AppLocalizations.of(context).translate('string_ten'),
                style: TextStyle(
                    color: Colors.blue[700],
                    fontFamily: 'Helvetica-Bold',
                    fontSize: 27
                ),
              ),
              SizedBox(height: 10),
              Text(
                AppLocalizations.of(context).translate('string_eleven'),
                style: TextStyle(
                    fontFamily: 'Helvetica',
                    fontSize: 18
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
  showAlertDialog1(BuildContext context) {
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text(
        AppLocalizations.of(context).translate('supported'),
        style: TextStyle(
            fontFamily: 'Helvetica-Bold',
            fontSize: 18
        ),
        //textAlign: TextAlign.center,
      ),
      content:
        Container(
          child:
            Text(
              AppLocalizations.of(context).translate('languages'),
              style: TextStyle(
              fontFamily: 'Helvetica',
              fontSize: 18
              ),
            ),
        ),
      actions: [
        okButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  showAlertDialog2(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content:
      Container(
        child:
        Text(
          'registertovote.ca.gov',
          style: TextStyle(
              fontFamily: 'Helvetica',
              fontSize: 18
          ),
        ),
      ),
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  showAlertDialog3(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content:
      Container(
        child:
        Text(
          'sos.ca.gov',
          style: TextStyle(
              fontFamily: 'Helvetica',
              fontSize: 18
          ),
        ),
      ),
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
